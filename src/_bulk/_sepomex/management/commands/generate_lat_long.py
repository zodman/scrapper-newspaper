from django.core.management.base import BaseCommand, CommandError
from sepomex.models import Sepomex
#import simplejson  as json
import demjson as json
from geopy.geocoders import GoogleV3
import time
from tqdm import tqdm

class Command(BaseCommand):


    def handle(self, *args, **options):
        geo = GoogleV3(api_key="AIzaSyDGYWMZsbXxBe34guqeYk4-y52URxnHVaE")
        for i in tqdm( Sepomex.objects.filter(lat_lng_municipio__exact="").values("estado","municipio").distinct()):
            text = u"{} {}".format(i["estado"],i["municipio"])
            res = geo.geocode(text)
            Sepomex.objects.filter(estado=i["estado"], municipio=i["municipio"]).update(lat_lng_municipio = "{},{}".format(res.latitude,res.longitude))
            
