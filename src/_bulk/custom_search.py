import gnp
import pprint
import newspaper
from rebulk import Rebulk
from pymongo import MongoClient
from pymongo.errors import DuplicateKeyError
import click

def insert(title, content, location, url, wordlist, image):
    scrapy = MongoClient().scrapy.items
    d = {'title':title, 'content':content, 'url':url, 'location':location, 'wordlist': wordlist, 'image':image}
    try:
        id = scrapy.insert_one(d)
        click.echo("inserting {}".format(id))
    except DuplicateKeyError:
        pass


def get_location():
    sepomex = MongoClient().sepomex.sepomex
    states = sepomex.distinct("d_estado")
    municipios = sepomex.aggregate([{"$group":{'_id':{"d_estado":'$d_estado',"D_mnpio":'$D_mnpio'}}}])
    bulk = Rebulk()
    for state in states:
        bulk.string(state.strip(), ignore_case=True)
    for m in municipios:
        municipio = m.get("_id").get("D_mnpio")
        estado = m.get("_id").get("d_estado")
        if municipio.lower() in ('ario', 'centro'):
            bulk.string(u" {} ".format(municipio), ignore_case=True)
        else:
            bulk.string(municipio, ignore_case=True)
    return bulk
bulk_location = get_location()

def parser_results(results, search_str):
    for i in results.get("stories"):
        title, link = i.get("title"),i.get("link")
        click.secho("{} {}".format(title, link), fg="blue")
        a = newspaper.Article(link)
        a.download()
        a.parse()
        finded = bulk_location.matches(a.text)
        if finded:
            res =  ",".join([i.value for i in finded])
            click.echo(u"finding location {}".format(res))
            insert(title, a.text, res, link, search_str)


def main():
    with open('bot/wordlist.txt') as f:
        for i in f.readlines():
            search_str = "{} Mexico".format(i.strip())
            click.secho("searching {}".format(search_str), fg="red")
            results = gnp.get_google_news_query(search_str)
            parser_results(results, search_str)

if __name__ == '__main__':
    main()
