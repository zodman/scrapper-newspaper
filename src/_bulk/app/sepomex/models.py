from __future__ import unicode_literals

from django.db import models

class Sepomex(models.Model):
    codigo = models.PositiveIntegerField()
    asenta = models.CharField(max_length=100)
    municipio = models.CharField(max_length=100)
    estado = models.CharField(max_length=100)


    @property
    def location(self):
        return u"{}, {}".format(self.estado, self.municipio)


    def __unicode__(self):
        return u"{} {} {}".format(self.estado, self.municipio, self.asenta)
