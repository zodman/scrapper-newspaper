from django.core.management.base import BaseCommand, CommandError
from sepomex.models import Sepomex
#import simplejson  as json
import demjson as json

class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('file', type=str)

    def handle(self, *args, **options):
        file_name = options['file']
        with open(file_name) as f:
            for i in f.readlines():
                dict_str = i.replace("db.sepomex.insert(",'').replace(");",'')
                obj = json.decode(dict_str)
                Sepomex.objects.create(
                        codigo=int(obj.get("d_codigo")),
                        asenta = obj.get("d_asenta"),
                        municipio= obj.get("D_mnpio"),
                        estado = obj.get("d_estado")

                )
