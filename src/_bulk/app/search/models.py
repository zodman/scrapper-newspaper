from django.db import models
from sepomex.models import Sepomex



class Entry(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    url = models.URLField(unique=True)
    wordlist = models.CharField(max_length=100)
    valid = models.NullBooleanField(default=None)
    image = models.URLField(null=True, blank=True)
    locations = models.ManyToManyField(Sepomex)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return u"{}".format(self.url)
