import gnp
import pprint
import sys
import newspaper
from rebulk import Rebulk
#from pymongo import MongoClient
#from pymongo.errors import DuplicateKeyError
from django.core.management.base import BaseCommand, CommandError
from sepomex.models import Sepomex
from search.models  import Entry
import click
import django.db.utils
from django.db.models import Q
from walrus import Walrus
from geopy import Nominatim
import geopy.exc

cache = Walrus().cache()
geolocator = Nominatim()

def insert(title, content, location, url, wordlist, loc, image):

    d = {'title':title, 'content':content, 'url':url,  'wordlist': wordlist, "image":image}

    try:
        entry = Entry.objects.create(**d)
        click.echo("inserting {}".format(url))
    except django.db.utils.IntegrityError:
        Entry.objects.filter(url=url).update(**d)
        entry = Entry.objects.get(url=url)
        click.echo("updating {}".format(url))

    entry.locations.set(loc)



def get_location():
    states = Sepomex.objects.all().values_list("estado", flat=True).distinct()
    municipios = Sepomex.objects.all().values_list("municipio", flat=True).distinct()
    bulk = Rebulk()
    for state in states:
        bulk.string(u" {}".format(state.strip()), ignore_case=True)
    for municipio in municipios:
        bulk.string(u" {}".format(municipio), ignore_case=True)
        #if municipio.lower() in ('ario', 'centro', 'naco','ures','xico', 'isla',
                #'mina',"ticul","apan","talas","casas",
                #):
            #bulk.string(u" {} ".format(municipio), ignore_case=True)
        #else:
            #bulk.string(municipio, ignore_case=True)
    return bulk
bulk_location = get_location()

def parser_results(results, search_str):
    for i in results.get("stories"):
        title, link = i.get("title"),i.get("link")
        click.secho("{} {}".format(title, link), fg="blue")
        a = newspaper.Article(link)
        a.download()
        a.parse()
        image = a.top_image
        finded = bulk_location.matches(a.text)
        locations_results = Sepomex.objects.none()
        if finded:
            for i in finded:
                value = i.value.strip()
                location_results = Sepomex.objects.filter(
                        Q(estado=value)|Q(municipio=value)
                    )
            insert(title, a.text,"", link,search_str,  location_results, image)


            #res =  ",".join(set([i.value for i in finded]))
            #click.echo(u"finding location {}".format(res))
            #try:
            #    loc = geolocator.geocode(res + " Mexico")
            #except geopy.exc.GeocoderUnavailable:
            #    loc = None
            #insert(title, a.text, res, link, search_str, loc)


def main(file):
    with open(file) as f:
        for i in f.readlines():
            search_str = "{} Mexico".format(i.strip())
            click.secho("searching {}".format(search_str), fg="red")
            res = cache.get(search_str)
            if res is None:
                click.echo("fetch ....")
                results = gnp.get_google_news_query(search_str)
                cache.set(search_str, results)
            else:
                results = res

            parser_results(results, search_str)

class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('file', type=str)

    def handle(self, *args, **options):
        f = options.get("file")
        main(f)
