from django.core.management.base import BaseCommand
from search.models import Entry
from sepomex.models import Sepomex
from rebulk import Rebulk
from geopy.geocoders import Nominatim
import geopy.exc

locator = Nominatim()


class Command(BaseCommand):


    def get_location(self):
        municipios = Sepomex.objects.all().values_list("municipio", flat=True).distinct()
        bulk = Rebulk()
        for municipio in municipios:
            bulk.string(u"{}".format(municipio), ignore_case=True)

        return bulk

    def update(self, entry, location):
        try:
            geo = locator.geocode(location)
        except geopy.exc.GeocoderUnavailable:
            geo = None

        if geo:
            entry.lat = geo.latitude
            entry.long = geo.longitude
            entry.location = location
            entry.save()


    def add_arguments(self, parser):
        parser.add_argument('entry_id', nargs='?', type=int)


    def handle(self, *args, **kwargs):
        rebulk = self.get_location()
        if kwargs["entry_id"]:
            entries = Entry.objects.filter(id=kwargs["entry_id"])
        else:
            entries = Entry.objects.all()
        
        for entry in entries:
            content = entry.content
            res = rebulk.matches(content)
            for r in res:
                sepomex = Sepomex.objects.filter(municipio__iexact=r.value)
                if sepomex.exists():
                    print (entry.title, sepomex[0].location)
                    location = sepomex[0].location
                    self.update(entry, location)
                    break
