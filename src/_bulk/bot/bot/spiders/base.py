import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.linkextractors.lxmlhtml import LxmlLinkExtractor
from bot.items import Item

class Base(CrawlSpider):
    name = ""
    allowed_domains = ["",]
    #start_urls = (    )
    rules = (Rule(LinkExtractor(),callback='parse_item',follow=True),)
    xpath_title =""
    xpath_content = ""
    xpath_date_str = ""

    def pre_process(self, response):
        return response

    def parse_item(self, response):
        response = self.pre_process(response)
        item = Item()
        title = response.xpath(self.xpath_title)
        content = response.xpath(self.xpath_content)
        if self.xpath_date_str != "":
            date_str = response.xpath(self.xpath_date_str)
            item["date"] = date_str.extract()


        item["title"] = title.extract()
        item["content"] = content.extract()

        item["url"] = response.url
        return item
