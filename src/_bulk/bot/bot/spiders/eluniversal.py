# -*- coding: utf-8 -*-
from base import Base

class UniversalSpider(Base):
    name = "eluniversal"
    allowed_domains = ["eluniversal.com.mx",]
    start_urls = (
        'http://www.eluniversal.com.mx/',
    )
    xpath_title = "//div[@class='pane-content']/h1/text()"
    xpath_content = "//div[@class='pane-content']/div[contains(@class,'field')]/p/text()"
    xpath_date_str ="//div[@class='fechap']/text()"
