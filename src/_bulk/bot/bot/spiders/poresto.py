# -*- coding: utf-8 -*-
from base import Base
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from date_range import date_range
import datetime


class DiariodeyucatanSpider(Base):
    name = "poresto"
    allowed_domains = ["poresto.net",]
    xpath_title = "//span[contains(@style,'font-size:26px;')]/text()"
    xpath_content = "//div[contains(@align,'justify')]/text()"
    xpath_date_str =""
    rules = (
            Rule(LinkExtractor(allow=("ver_nota\.php",)),callback='parse_item',follow=True),
            )
    def start_requests(self):
        url = "http://www.poresto.net/yucatan.php?fecha={}"
        for i in date_range(datetime.date(2014,1,1), datetime.date.today()):
            url_ = url.format(i.strftime("%Y-%m-%d"))
            print url_
            yield scrapy.Request(url_)
