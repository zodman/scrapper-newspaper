# -*- coding: utf-8 -*-
from base import Base
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

class ElPeriodicoSpider(Base):
    name = "elperiodicodearagon"
    allowed_domains = ["elperiodicodearagon.com",]
    start_urls = (
        'http://www.elperiodicodearagon.com/',
        "http://www.elperiodicodearagon.com/noticias/aragon/fallece-uno-cinco-afectados-comer-jabali-triquinosis_651514.html",
    )
    xpath_title = "//div[@class='Contenidos  Noticia']/h1/text()"
    xpath_content = "//div[@id='CuerpoDeLaNoticia']/p/text()"
    xpath_date_str ="//div[@class='fechap']/text()"

    rules = (
            Rule(LinkExtractor(allow=("noticias",)),callback='parse_item',follow=True),
            )
