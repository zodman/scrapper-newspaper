# -*- coding: utf-8 -*-
from base import Base

class DiariodeyucatanSpider(Base):
    name = "diariodeyucatan"
    allowed_domains = ["yucatan.com.mx",]
    start_urls = (
        'http://www.yucatan.com.mx/',
        'http://yucatan.com.mx/yucatan/brote-de-salmonelosis',
    )
    xpath_title = "//div[contains(@class,'blog-post')]/h1[contains(@class,'post-tile')]/text()"
    xpath_content = "//div[contains(@class,'blog-post')]/div/div/p/text()"
    xpath_date_str ="//div[contains(@class,'blog-post')]/span[@class='entry-date']/@datetime"
