# -*- coding: utf-8 -*-
from base import Base

class SipseSpider(Base):
    name = "sipse"
    allowed_domains = ["sipse.com",]
    start_urls = (
        'http://www.sipse.com/',
    )
    xpath_title = "//article/header[contains(@class,'main')]/h1/text()"
    xpath_content = "//article/div[contains(@class,'text')]/p/text()"
    xpath_date_str ="//article/header//div[@class='fecha']/time/@datetime"
