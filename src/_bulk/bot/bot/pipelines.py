# -*- coding: utf-8 -*-
from scrapy.exceptions import DropItem, CloseSpider
from pymongo import MongoClient
from rebulk import Rebulk
import re

class MissingPipeline(object):
    def process_item(self, item, spider):
        if not item["content"]:
            raise DropItem("Missing content  in %s" % item["url"])
        if not item["title"]:
            raise DropItem("Missing title  in %s" % item["url"])
        return item

class DuplicatesPipeline(object):
    def open_spider(self,spider):
        client = MongoClient()
        self.db = client.scrapy.get_collection("items")

    def process_item(self, item, spider):
        if self.db.find({'url': item['url']}).count() >0 :
            raise DropItem("Duplicate url found: %s" % item)
        return item


class WordListPipeline(object):
    def open_spider(self,spider):
        client = MongoClient()
        self.db = client.wordlist.get_collection("word")
        wordlist = self.db.distinct("name")
        self.bulk = Rebulk()
        for word in wordlist:
            self.bulk.string( u" {} ".format(word), ignore_case=True)

    def process_item(self, item, spider):
        text = u" ".join(item["content"])
        text  = re.sub(r"\s", "", text)
        result = self.bulk.matches(text)
        if not result:
            raise DropItem("nothing found")
        else:
            res =  ",".join([i.value for i in result])
            item["wordlist"] = res
            return item


class FindLocationPipeline(object):
    def open_spider(self,spider):
        self.sepomex = MongoClient().sepomex.sepomex
        states = self.sepomex.distinct("d_estado")
        #muni = self.sepomex.distinct("D_mnpio")
        self.bulk = Rebulk()
        for state in states:
            self.bulk.string(state, ignore_case=True)

    def process_item(self, item, spider):
        text = u" ".join(item["content"])
        text  = re.sub(r"\s", "", text)
        result = self.bulk.matches(text)
        if not result:
            raise DropItem("nothing location")
        else:
            res =  ",".join([i.value for i in result])
            item["location"] = res
            return item
