#!/usr/bin/env python

import sys
import getpass
import telnetlib

HOST = "localhost"

def main():
    ports = ['6023']
    if len(sys.argv) > 1:
        ports = sys.argv[1].split(',')

    for port in ports:
        get_stats(port)

def get_stats(port):
    tn = telnetlib.Telnet(HOST, port)
    tn.read_until('>>>')
    tn.write("spider.name\n")
    tn.read_until('>>>')
    tn.write("est()")
    print tn.read_until('>>>')
    tn.close()

if __name__ == '__main__':
    main()
