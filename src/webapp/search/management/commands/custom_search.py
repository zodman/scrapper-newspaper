# encoding=utf8
import gnp
import pprint
import sys
import newspaper
from rebulk import Rebulk
#from pymongo import MongoClient
#from pymongo.errors import DuplicateKeyError
from django.core.management.base import BaseCommand, CommandError
from sepomex.models import MXMunicipio, MXEstado
from search.models  import Entry
import click
import django.db.utils
from django.db.models import Q
from walrus import Walrus
from geopy import Nominatim
import geopy.exc
from wordlist.models import Word

cache = Walrus().cache()
geolocator = Nominatim()

def insert(title, content, location, url, wordlist, loc, image):

    d = {'title':title, 'content':content, 'url':url,  'wordlist': wordlist,
            "image":image,'locations_text':location}

    try:
        entry = Entry.objects.create(**d)
        click.echo("inserting {}".format(url))
    except django.db.utils.IntegrityError:
        Entry.objects.filter(url=url).update(**d)
        entry = Entry.objects.get(url=url)
        click.echo("updating {}".format(url))
    locations, type_ = loc
    if "mun" == type_:
        entry.locations.set(locations)
    else:
        entry.locations_state.set(locations)



def get_location():
    states = MXEstado.objects.all().values_list("nombre", flat=True).distinct()
    municipios = MXMunicipio.objects.all().values_list("nombre", flat=True).distinct()
    bulk = Rebulk()
    for state in states:
        bulk.string(u" {}".format(state.strip()), ignore_case=True)
    for municipio in municipios:
        bulk.string(u" {}".format(municipio), ignore_case=True)
        #if municipio.lower() in ('ario', 'centro', 'naco','ures','xico', 'isla',
                #'mina',"ticul","apan","talas","casas",
                #):
            #bulk.string(u" {} ".format(municipio), ignore_case=True)
        #else:
            #bulk.string(municipio, ignore_case=True)
    return bulk
bulk_location = get_location()

def parser_results(results, search_str):
    for i in results.get("stories"):
        title, link = i.get("title"),i.get("link")
        click.secho("{} {}".format(title, link), fg="blue")
        a = newspaper.Article(link)
        a.download()
        a.parse()
        image = a.top_image
        finded = bulk_location.matches(a.text)
        locations_results = MXMunicipio.objects.none()
        if finded:

            for i in finded:
                value = i.value.strip()
                location_results = MXMunicipio.objects.filter(
                        nombre=value
                    )
                type_ = "mun"
                if not location_results.exists():
                    location_results = MXEstado.objects.filter(
                            nombre=value
                        )
                    type_ = "est"
            finded_list = set([i.value for i in finded])
            location_text = ",".join([i for i in finded_list])
            insert(title, a.text,location_text, link,search_str, (location_results,type_), image)


            #res =  ",".join(set([i.value for i in finded]))
            #click.echo(u"finding location {}".format(res))
            #try:
            #    loc = geolocator.geocode(res + " Mexico")
            #except geopy.exc.GeocoderUnavailable:
            #    loc = None
            #insert(title, a.text, res, link, search_str, loc)


def main():
    for i in Word.objects.all().values_list("text", flat=True):
        search_str = u"{} Mexico".format(i.strip())
        click.secho(u"searching {}".format(search_str), fg="red")
        res = cache.get(search_str)
        if res is None:
            click.echo("fetch ....")
            results = gnp.get_google_news_query(search_str.encode("latin1"))
            cache.set(search_str, results)
        else:
            results = res

        parser_results(results, search_str)

class Command(BaseCommand):



    def handle(self, *args, **options):
        main()
