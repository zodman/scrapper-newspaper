from django.conf.urls import include, url
from .views import home, detail, map_view, detail_json, map_view_json

urlpatterns = [
    url(r"^home$", home, name="start"),
    url(r"^map$", map_view, name="map"),
    
    url(r"^detail/json$", detail_json, name="detail_json"),
    url(r"^detail/(?P<pk>\d+)$", detail, name="detail"),

    url(r"^api/map$", map_view_json, name="map_json"),
    url(r"^api/detail$", detail_json, name="map_detail_json"),
    
]
