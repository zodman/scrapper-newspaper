from sepomex.models  import MXEstado

def global_estados(request):

    estado_id = request.GET.get("estado",None)
    if not estado_id  is None:
        request.session["estado"] = int(estado_id)

    estados = MXEstado.objects.all()
    return {'global_estados':estados}