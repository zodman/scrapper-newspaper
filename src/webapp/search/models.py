from django.db import models
from django.db.models import Q
from sepomex.models import MXMunicipio as Sepomex, MXEstado

class EntryManager(models.Manager):
    def get_queryset(self):
        qs = super(EntryManager, self).get_queryset()
        qs = qs.filter(Q(valid=True)|Q(valid=None))
        return qs

class Entry(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    url = models.URLField(unique=True)
    wordlist = models.CharField(max_length=100)
    valid = models.NullBooleanField(default=None)
    image = models.URLField(null=True, blank=True)
    locations = models.ManyToManyField(Sepomex)
    locations_state = models.ManyToManyField(MXEstado)
    locations_text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = EntryManager()
    
    

    def __unicode__(self):
        return u"{}".format(self.url)

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('detail', kwargs={'pk':self.id})

    def get_location(self):
        result = ""
        for i in self.locations.all():
            result += u"{},{}".format(i.nombre, i.mx_estado.nombre)
        return result
