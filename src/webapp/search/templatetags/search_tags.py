from django import template
from django.utils.safestring import mark_safe
register = template.Library()

@register.filter
def highlight(text, words):
    str_ = ""
    for word in words.split(","):
        word = word.strip()
        str_ = mark_safe(text.replace(word, "<span class='highlight'>%s</span>" % word))
    return str_


@register.filter
def highlight2(text, word):
    word = word.replace("Mexico","")
    return mark_safe(text.replace(word, "<span class='highlight2'>%s</span>" % word))
