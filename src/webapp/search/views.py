from django.views.generic import TemplateView, ListView, DetailView
from django.db.models import Q
from .models import Entry
import json
from sepomex.models import MXMunicipio as Sepomex
from django.http import JsonResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse

filters = ()

def map_view_json(request):
    context = {}
    locations = Entry.objects.exclude(
                Q(locations__lat_lng="")|
                Q(locations_state__lat_lng="")
                ).values(
                'id',"locations__lat_lng","locations_state__lat_lng").distinct()
    markers = []
    for i in locations:
        if i["locations__lat_lng"] is None:
            continue
        lat, lng = i["locations__lat_lng"].split(",")
        marker = {
                    "position":[lat,lng],
                    'data':{'id':i["id"]}
                }
        markers.append(marker)
    context.update({'markers':markers})
    return JsonResponse(context)


class BaseV(ListView):
    model = Entry
    paginate_by = 25

    def get_queryset(self):
        q =  self.request.GET.get("q", None)
        if q is None:
            return self.model.objects.none()
        else:
            qs = super(BaseV,self).get_queryset()
            qs = qs.filter(Q(title__icontains=q)|Q(locations__nombre__icontains=q)|Q(locations__mx_estado__nombre__icontains=q))

        estados = self.request.GET.getlist("estados")
        if estados:
            qs = qs.filter(locations__mx_estado__id__in=estados)

        return qs 

    def get_context_data(self, **context):
        context = super(BaseV,self).get_context_data(**context)
        markers = []
        qs = self.get_queryset()
        locations = qs.exclude(
                Q(locations__lat_lng="")|
                Q(locations_state__lat_lng="")
                ).values(
                'id',"locations__lat_lng","locations_state__lat_lng").distinct()

        for i in locations:
            if not i["locations__lat_lng"] is None:
                lat, lng = i["locations__lat_lng"].split(",")
                marker = {
                            "position":[lat,lng],
                            'data':{'id':i["id"]}
                        }
                markers.append(marker)

            if not  i["locations_state__lat_lng"] is None:
                lat, lng = i["locations_state__lat_lng"].split(",")
                marker = {
                            "position":[lat,lng],
                            'data':{'id':i["id"]}
                        }
                markers.append(marker)
        context.update({'markers':json.dumps(markers),
                        'q':self.request.GET.get("q",""),
                        'estados':self.request.GET.getlist("estados"),
                        'search':self.request.GET.get("search")
                        })
        return context

class Map(BaseV):
    template_name = "map.html"

map_view = Map.as_view()

class Home(BaseV):
    template_name="home.html"


home = Home.as_view()

class Detail(DetailView):
    model = Entry

detail = Detail.as_view()

class DetailJSON(TemplateView):
    def render_to_response(self, context, **response_kwargs):
        return self.render_to_json_response(context, **response_kwargs)
    def render_to_json_response(self, context, **response_kwargs):
        """
        Returns a JSON response, transforming 'context' to make the payload.
        """
        return JsonResponse(
            self.get_data(context),
            **response_kwargs
        )

    def get_data(self, context):
        """
        Returns an object that will be serialized as JSON by json.dumps().
        """
        id = self.request.GET.get("id")
        entry_obj = Entry.objects.filter(id=id)
        entry = entry_obj.values()
        context.update(entry[0])
        del context["view"]
        context["content"] = context["content"][0:135]
        context["pub_url"] = entry_obj[0].get_absolute_url()
        return context

detail_json = DetailJSON.as_view()

