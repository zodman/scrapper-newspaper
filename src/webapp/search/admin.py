from django.contrib import admin
from .models import Entry

class EntryAdmin(admin.ModelAdmin):
    list_display = ('title','wordlist', 'valid')
    raw_id_fields  = ("locations","locations_state")
admin.site.register(Entry,EntryAdmin)
