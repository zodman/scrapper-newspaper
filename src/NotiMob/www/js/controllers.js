angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {})

.controller('MapCtrl', function($scope, $state, $cordovaGeolocation, Markers, $http,$document, ApiEndPoint) {
   
 var options = {timeout: 10000, enableHighAccuracy: true};
 
var source   = angular.element(document.querySelector("#entry-template")).html();
var template = Handlebars.compile(source);
 
     
    var mapOptions = {
       center :{lat:23.386092,lng:-111.5558421},
      zoom:4,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
 
    $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);

    Markers.all().then(function(response){
        angular.forEach(response, function(value, key){
            var mark = {
            position:{
                    lat:parseFloat(value.position[0]),
                    lng:parseFloat(value.position[1])
            },
            map: $scope.map,
            title: 'pos '+ value.data.id
           }
           var marker = new google.maps.Marker(mark);
           marker.addListener("click", function(){
                $http.get(ApiEndPoint.url + "/detail" + "?id="+value.data.id).then(function(response){
                        var infowin = new google.maps.InfoWindow();
                          if (infowin) {
                            infowin.open($scope.map, marker);
                            data = response.data;
                            data["base_url"]=ApiEndPoint.base_url;
                            var html = template(data);
                            //console.log("response",response, html)
                            infowin.setContent(html);
                          }

                });
              
           });
        });
    });
   
 

})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
